import { useEffect, useState } from "react";
import {
  StyleHeaderContainer,
  StyleHeaderLogo,
  StyleNavWrapper,
  StyleNavItem,
  StyleNavItemText,
  StyleFeatures,
  StyleTranslate,
  StyleSearch,
  StyleNav,
  StyleMenu,
  StyleImage,
} from "./styles";
import Logo from "assets/images/Logo.svg";
import Icon from "assets/images/DownArrow.svg";
import Translate from "assets/images/Translate.svg";
import Search from "assets/images/Search.svg";
import Menu from "assets/images/menu.svg";
import { useNavigate } from "react-router-dom";
const Header = () => {
  const navigate = useNavigate()
  function getWindowSize() {
    const { innerWidth } = window;
    return { innerWidth };
  }
  const [windowSize, setWindowSize] = useState(getWindowSize());
  useEffect(() => {
    function handleWindowResize() {
      setWindowSize(getWindowSize());
    }
    window.addEventListener("resize", handleWindowResize);
    return () => {
      window.removeEventListener("resize", handleWindowResize);
    };
  }, []);
  return (
    <StyleHeaderContainer>
      <StyleNav>
        {windowSize.innerWidth < 1024 ? (
          <>
            <StyleMenu width={24} height={24} src={Menu} alt="" />
            <StyleHeaderLogo>
             
                <StyleImage alt="Logo" src={Logo} />
             
            </StyleHeaderLogo>{" "}
            <StyleFeatures>
              <StyleSearch>
                <img alt="icon" className="search" src={Search} />
              </StyleSearch>
            </StyleFeatures>
          </>
        ) : (
          <>
            <StyleHeaderLogo>
               <div style={{
                fontSize:"20px", cursor:"pointer"
               }} onClick={() => navigate('/')}>Be Nguyen spa</div>
            </StyleHeaderLogo>
            <StyleNavWrapper>
              <StyleNavItem>
                <StyleNavItemText onClick={() => navigate('/')}>Trang chủ</StyleNavItemText>
              </StyleNavItem>
              <StyleNavItem>
                <StyleNavItemText onClick={() => navigate('/about')}>Giới thiệu</StyleNavItemText>
              </StyleNavItem>
              <StyleNavItem>
                <StyleNavItemText onClick={() => navigate('/service')}>Blog</StyleNavItemText>
              </StyleNavItem>
              <StyleNavItem>
                <StyleNavItemText onClick={() => navigate('/contact')}>Liên hệ</StyleNavItemText>
              </StyleNavItem>
            </StyleNavWrapper>
            <StyleFeatures>
              <StyleTranslate>
                <img alt="icon" className="translate" src={Translate} />
              </StyleTranslate>
              <StyleSearch>
                <img alt="icon" className="search" src={Search} />
              </StyleSearch>
            </StyleFeatures>
          </>
        )}
      </StyleNav>
    </StyleHeaderContainer>
  );
};

export default Header;
