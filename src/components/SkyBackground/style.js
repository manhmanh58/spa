import styled from "styled-components";

const StyleSkyBackground = styled.div`
  position: relative;
  background-color: rgba(229, 249, 255, 1);
`;

export { StyleSkyBackground };
