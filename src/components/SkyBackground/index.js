import React from "react";
import { StyleSkyBackground } from "./style";

const SkyBackground = ({ children }) => {
  return <StyleSkyBackground>{children}</StyleSkyBackground>;
};

export default SkyBackground;
