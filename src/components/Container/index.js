import React from "react";
import { StyleContainer } from "./style";

const Container = ({ children }) => {
  return <StyleContainer>{children}</StyleContainer>;
};

export default Container;
