import styled from "styled-components";
import { devices } from "assets/style/responsive";
const StyleContainer = styled.div`
  margin: auto;
  max-width: 1280px;
  padding: 0 24px;
  @media ${devices.desktop} {
    padding: 0px 16px;
  }
  @media ${devices.ipad} {
    padding: 0px 24px;
  }
`;

export { StyleContainer };
