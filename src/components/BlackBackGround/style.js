import styled from "styled-components";
import { devices } from "assets/style/responsive";

const StyleBlackBackGround = styled.div`
  background-color: #001e26;
  color: #fff;
  position: relative;
  margin-top: ${(props) => (props.mgTop ? props.mgTop : "")};
  @media ${devices.iphone} {
    margin-top: -56px;
  }
`;

export { StyleBlackBackGround };
