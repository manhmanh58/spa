import { StyleBlackBackGround } from "./style";

const BlackBackGround = ({ children, mgTop }) => {
  return <StyleBlackBackGround mgTop={mgTop}>{children}</StyleBlackBackGround>;
};

export default BlackBackGround;
