import styled from "styled-components";

const StyleLightBackground = styled.div`
  background-color: rgba(1, 42, 53, 1);
  color: #fff;
  position: relative;
`;

const StyleContainer = styled.div`
  margin: auto;
  max-width: 1280px;
  padding: 0 24px;
  padding: 0px 16px;
  img {
    width: 100%;
    position: absolute;
    object-fit: fill;
    display: block;
    left: 0;
    bottom: 0;
    transform: translatey(50%);
  }
`;

export { StyleLightBackground, StyleContainer };
