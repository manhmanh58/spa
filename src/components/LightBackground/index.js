import { StyleLightBackground } from "./style";

const LightBackground = ({ children }) => {
  return <StyleLightBackground>{children}</StyleLightBackground>;
};

export default LightBackground;
