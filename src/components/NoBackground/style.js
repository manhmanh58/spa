import styled from "styled-components";

const StyleNoBackground = styled.div`
  position: relative;
`;

export { StyleNoBackground };
