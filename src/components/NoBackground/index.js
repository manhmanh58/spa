import React from "react";
import { StyleNoBackground } from "./style";

const Nobackground = ({ children }) => {
  return <StyleNoBackground>{children}</StyleNoBackground>;
};

export default Nobackground;
