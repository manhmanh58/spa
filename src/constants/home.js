const NUXT_LINK = "https://v3.nuxtjs.org/";
const FEATURES_IMAGE =
  "https://nuxtjs.org/img/home/discover/dx/discover-mountain.svg";
const PARTNERS_IMAGE =
  "https://nuxtjs.org/img/home/discover/partners/partners-illustration.svg";
const GUIDE_IMAGE = "https://nuxtjs.org/img/home/explore/landscape-explore.svg";
const EXPLORE_IMAGE =
  "https://nuxtjs.org/img/home/campfire/campfire-illustration-big.svg";
const TESTIMONIALS_IMAGE = "https://nuxtjs.org/img/home/home_footer.svg";

export {
  NUXT_LINK,
  FEATURES_IMAGE,
  PARTNERS_IMAGE,
  GUIDE_IMAGE,
  EXPLORE_IMAGE,
  TESTIMONIALS_IMAGE,
};
