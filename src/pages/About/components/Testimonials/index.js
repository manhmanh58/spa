import React from "react";
import {
  StyleWrapper,
  StyleContainer,
  StyleAvatar,
  StyleCard,
  StyleCompany,
  StyleInfo,
  StyleProfile,
} from "./style";
import {
  StyleTitle,
  StyleDescript,
  StyleContents,
} from "assets/style/globalstyle";
import { TESTIMONIALS } from "constants/staticData";

const Testimonials = () => {
  return (
    <StyleWrapper>
      <StyleTitle>Community</StyleTitle>
      <StyleDescript>Testimonials</StyleDescript>
      <StyleContents>Learn what the experts love about Nuxt.</StyleContents>
      <StyleContainer>
        {TESTIMONIALS.map((tes, index) => (
          <StyleCard key={index}>
            <p>{tes.des}</p>
            <StyleInfo>
              <StyleAvatar src={tes.avatar} alt="" />
              <StyleProfile>
                <p style={{ fontWeight: "700", fontSize: "18px" }}>
                  {tes.name}
                </p>
                <p
                  style={{ color: "rgba(178, 204, 204, 1)", fontSize: "14px" }}
                >
                  {tes.title}
                </p>
              </StyleProfile>
              <StyleCompany src={tes.in} alt="" />
            </StyleInfo>
          </StyleCard>
        ))}
      </StyleContainer>
    </StyleWrapper>
  );
};

export default Testimonials;
