import styled from "styled-components";
import { devices } from "assets/style/responsive";
const StyleWrapper = styled.div`
  text-align: center;
  padding-top: 5rem;
  padding-bottom: 17rem;
`;

const StyleContainer = styled.div`
  display: grid;
  margin-top: 50px;
  column-gap: 3%;
  row-gap: 48px;
  grid-template-columns: auto auto auto;
  margin-bottom: 200px;
  @media ${devices.ipad} {
    grid-template-columns: auto auto auto;
    row-gap: 20px;
  }
  @media ${`(max-width: 767px)`} {
    grid-template-columns: auto;
    row-gap: 20px;
  }
  @media ${devices.iphone} {
    grid-template-columns: auto;
    row-gap: 20px;
  }
`;
const StyleCard = styled.div`
  border: 1px solid #cecece;
  padding: 20px;
  border-radius: 6px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
const StyleAvatar = styled.img`
  width: 48px;
  height: 48px;
`;
const StyleCompany = styled.img`
  width: 28px;
  height: 28px;
  margin-left: auto;
`;
const StyleInfo = styled.div`
  display: flex;
  align-items: center;
  margin-top: 20px;
`;
const StyleProfile = styled.div`
  margin-left: 20px;
  text-align: start;
  p + p {
    margin-top: 8px;
  }
`;
export {
  StyleWrapper,
  StyleContainer,
  StyleCard,
  StyleAvatar,
  StyleCompany,
  StyleInfo,
  StyleProfile,
};
