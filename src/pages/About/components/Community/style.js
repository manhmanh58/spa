import styled from "styled-components";
import { devices } from "assets/style/responsive";
const StyleWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  text-align: center;
  padding-top: 9rem;
  padding-bottom: 14rem;
  position: relative;
  z-index: 10;
  align-items: start;
`;
const StyleContent = styled.div`
  text-align: left;
  display: flex;
  flex-direction: column;
  p {
    margin: 0;
    margin-bottom: 16px;
  }
  > div {
    overflow: hidden;
    border-radius: 8px;
    img {
      max-width: 600px;
      width: 100%;
      height: 100%;
      object-fit: cover;
      aspect-ratio: 16 / 9;
    }
  }
`;
const StyleName = styled.p`
  color: #00dc82;
  font-size: 1.125rem;
  line-height: 1.75rem;
  font-weight: 700;
`;
const StyleContainer = styled.div`
  display: grid;
  grid-gap: 5rem;
  gap: 5rem;
  margin-top: 100px;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  @media ${devices.lgPhone} {
    grid-template-columns: auto;
  }
`;

const StyleLink = styled.p`
  display: flex;
  align-items: center;
  width: fit-content;
  color: #00dc82;
  font-size: 18px;
  position: relative;
  cursor: pointer;
  svg {
    margin-left: 5px;
  }
  &::before {
    content: "";
    display: block;
    position: absolute;
    bottom: -12px;
    width: 40%;
    border-bottom: 1px solid #00dc82;
  }

  &:hover {
    &::before {
      width: 100%;
    }
  }
`;

const StyleText = styled.p`
  font-size: 1.25rem;
  line-height: 1.75rem;
  font-weight: 700;
`;
export {
  StyleWrapper,
  StyleContent,
  StyleContainer,
  StyleLink,
  StyleText,
  StyleName,
};
