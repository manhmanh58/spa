import React from "react";
import { StylePlanet } from "./style";
import { Planets } from "constants/planets";

const Planet = () => {
  return (
    <StylePlanet>
      {Planets.map((Planet, index) => (
        <img key={index} src={Planet.src} alt="i"></img>
      ))}
    </StylePlanet>
  );
};

export default Planet;
