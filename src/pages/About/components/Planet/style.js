import styled from "styled-components";
import { devices } from "assets/style/responsive";
const StylePlanet = styled.div`
  position: absolute;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  top: 80px;

  img {
    z-index: 12;
    position: absolute;
    &:nth-of-type(1) {
      transform: translateX(-8.02px) translateY(2.72px);
      left: 100px;
      top: 35px;
      @media ${devices.desktop} {
        display: none;
      }
    }
    &:nth-of-type(2) {
      transform: translateX(48.4px) translateY(23.79px);
      right: 250px;
      top: 25px;
      @media ${devices.desktop} {
        right: 150px;
      }
      @media ${devices.lgPhone} {
        display: none;
      }
    }
    &:nth-of-type(3) {
      transform: translateX(32.28px) translateY(17.77px);
      left: 0px;
      top: 28%;
      @media ${devices.desktop} {
        left: 125px;
        transform: translateX(-32.28px);
        top: 75px;
      }
      @media ${devices.lgPhone} {
        left: 50%;
        transform: translateX(-32.28px);
        top: 50px;
      }
    }
    &:nth-of-type(4) {
      transform: translateX(0.04px) translateY(5.73px);
      right: 0px;
      top: 28%;
      @media ${devices.desktop} {
        display: none;
      }
    }
    &:nth-of-type(5) {
      transform: translateX(48.4px) translateY(23.79px);
      left: 100px;
      bottom: 200px;
      @media ${devices.desktop} {
        bottom: 150px;
      }
      @media ${devices.lgPhone} {
        bottom: 70px;
        left: 50px;
      }
    }
    &:nth-of-type(6) {
      transform: translateX(24.22px) translateY(14.76px);
      right: 200px;
      bottom: 200px;
      @media ${devices.desktop} {
        right: 150px;
        bottom: 150px;
      }
      @media ${devices.lgPhone} {
        bottom: 70px;
        right: 20px;
      }
    }
  }
`;

export { StylePlanet };
