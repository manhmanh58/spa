import styled from "styled-components";
import { devices } from "assets/style/responsive";

const StyleContent = styled.div`
  padding: 24px;
  cursor: pointer;
  border-radius: 0.375rem;
  p {
    margin-top: 20px;
    @media ${devices.ipad} {
      font-size: 18px;
    }
    @media ${devices.iphone} {
      font-size: 16px;
    }
  }
  &:hover {
    background-color: rgba(14, 165, 233, 0.5);
  }
`;

const StyleContainer = styled.div`
  display: grid;
  grid-gap: 5rem;
  gap: 5rem;
  margin-top: 100px;
  grid-template-columns: repeat(3, minmax(0, 1fr));
  @media ${devices.lgPhone} {
    margin: 0;
    grid-template-columns: auto;
  }
`;

export { StyleContent, StyleContainer };
