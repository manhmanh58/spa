import styled from "styled-components";
import { devices } from "assets/style/responsive";

const StyleContainer = styled.div`
  display: grid;
  gap: 2%;
  margin-top: 100px;
  grid-template-columns: repeat(4, minmax(0, 1fr));
  @media ${devices.lgPhone} {
    margin: 0;
    grid-template-columns: auto auto;
  }
`;
const StyleContent = styled.div`
  padding: 5%;
  cursor: pointer;
  border-radius: 0.375rem;
  p {
    margin-top: 20px;
    font-size: 1rem;
    line-height: 1.5rem;
    @media ${devices.ipad} {
      font-size: 1rem;
    }
    @media ${devices.iphone} {
      font-size: 0.875rem;
      line-height: 1.25rem;
      margin: 0;
    }
  }
  &:hover {
    background-color: rgba(0, 53, 67, 1);
  }
`;
const StyleText = styled.p`
  font-size: 1.25rem;
  line-height: 1.75rem;
  font-weight: 700;
`;

export { StyleContainer, StyleContent, StyleText };
