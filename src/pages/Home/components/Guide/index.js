import React from "react";
import { StyleContainer, StyleContent, StyleText } from "./style";
import { GUIDES } from "constants/staticData";
import { StyleWrapper } from "assets/style/globalstyle";

import {
  StyleTitle,
  StyleDescript,
  StyleContents,
} from "assets/style/globalstyle";
const Guide = () => {
  return (
    <StyleWrapper>
      <StyleTitle>Learn </StyleTitle>
      <StyleDescript>
        Follow our <b> Guides</b>
      </StyleDescript>
      <StyleContents>
        From an idea to a masterpiece, guides take you on the path to becoming a
        Nuxter.
      </StyleContents>
      <StyleContainer>
        {GUIDES.map((guide, index) => (
          <StyleContent key={index}>
            <img src={guide.img} alt="" />
            <StyleText>{guide.title}</StyleText>
            <p>{guide.des}</p>
          </StyleContent>
        ))}
      </StyleContainer>
    </StyleWrapper>
  );
};

export default Guide;
