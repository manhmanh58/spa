import React from "react";
import { StyleContent, StyleText } from "./style";
import { FEATURES } from "constants/staticData";
import {
  StyleTitle,
  StyleDescript,
  StyleContents,
  StyleWrapper,
} from "assets/style/globalstyle";

const Features = () => {
  return (
    <StyleWrapper>
      <StyleTitle>Dịch vụ</StyleTitle>
      <StyleDescript>
        Trải <b>N</b>ghiệm D<b>ịch</b> vụ
      </StyleDescript>
      <StyleContents>
        Tìm phương pháp điều trị hoàn hảo cho các nhu cầu cụ thể của bạn
      </StyleContents>
      <StyleContent>
        {FEATURES.map((feat, index) => (
          <div key={index}>
            <img
              style={{ width: "250px", height: "150px" }}
              src={feat.img}
              alt=""
            />
            <StyleText>{feat.title}</StyleText>
            <p style={{ fontSize: "16px" }}>{feat.des}</p>
          </div>
        ))}
      </StyleContent>
    </StyleWrapper>
  );
};

export default Features;
