import React from "react";
import { StyleStar, StyleWrapper } from "./style";

const Star = () => {
  return (
    <StyleWrapper>
      <StyleStar
        data-v-c479c21e
        style={{
          top: 92.9818,
          left: 203.17,
          height: 2,
          width: 2,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 835.583,
          left: 986.369,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 289.68,
          left: 645.274,
          height: 2,
          width: 2,
          animationDuration: "0s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 34.8853,
          left: 1234.18,
          height: 1,
          width: 1,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 620.92,
          left: 452.609,
          height: 0,
          width: 0,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 58.8144,
          left: 1474.16,
          height: 4,
          width: 4,
          animationDuration: "6s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 662.867,
          left: 1269.88,
          height: 3,
          width: 3,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 887.411,
          left: 1197.65,
          height: 3,
          width: 3,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 292.146,
          left: 1384.68,
          height: 2,
          width: 2,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 925.986,
          left: 845.303,
          height: 3,
          width: 3,
          animationDuration: "7s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 146.957,
          left: 844.242,
          height: 1,
          width: 1,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 543.164,
          left: 455.547,
          height: 1,
          width: 1,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 605.565,
          left: 113.264,
          height: 2,
          width: 2,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 935.596,
          left: 1434.41,
          height: 3,
          width: 3,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 47.9187,
          left: 703.141,
          height: 1,
          width: 1,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 102.018,
          left: 575.174,
          height: 3,
          width: 3,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 209.601,
          left: 763.851,
          height: 1,
          width: 1,
          animationDuration: "8s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 176.772,
          left: 1344.21,
          height: 4,
          width: 4,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 302.928,
          left: 444.969,
          height: 2,
          width: 2,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 420.673,
          left: 460.654,
          height: 3,
          width: 3,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 16.5245,
          left: 648.087,
          height: 3,
          width: 3,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 221.109,
          left: 1293.22,
          height: 2,
          width: 2,
          animationDuration: "6s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 76.0555,
          left: 705.638,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 416.885,
          left: 413.753,
          height: 1,
          width: 1,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 627.092,
          left: 883.103,
          height: 3,
          width: 3,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 154.628,
          left: 681.477,
          height: 4,
          width: 4,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 30.6899,
          left: 800.122,
          height: 2,
          width: 2,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 538.342,
          left: 336.759,
          height: 2,
          width: 2,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 354.407,
          left: 1188.93,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 431.278,
          left: 1392.95,
          height: 0,
          width: 0,
          animationDuration: "7s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 458.866,
          left: 591.327,
          height: 1,
          width: 1,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 910.976,
          left: 448.895,
          height: 1,
          width: 1,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 274.173,
          left: 163.084,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 34.5891,
          left: 451.002,
          height: 0,
          width: 0,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 849.484,
          left: 579.237,
          height: 1,
          width: 1,
          animationDuration: "7s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 31.6735,
          left: 1424.45,
          height: 2,
          width: 2,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 349.577,
          left: 256.561,
          height: 1,
          width: 1,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 679.952,
          left: 234.079,
          height: 2,
          width: 2,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 172.595,
          left: 407.578,
          height: 1,
          width: 1,
          animationDuration: "8s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 554.305,
          left: 979.484,
          height: 2,
          width: 2,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 153.843,
          left: 957.942,
          height: 0,
          width: 0,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 742.837,
          left: 422.785,
          height: 3,
          width: 3,
          animationDuration: "7s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 648.381,
          left: 854.298,
          height: 2,
          width: 2,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 113.488,
          left: 1376.05,
          height: 0,
          width: 0,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 21.1066,
          left: 1226.91,
          height: 2,
          width: 2,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 101.706,
          left: 795.818,
          height: 3,
          width: 3,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 311.913,
          left: 1305.97,
          height: 1,
          width: 1,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 848.332,
          left: 97.7268,
          height: 1,
          width: 1,
          animationDuration: "6s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 238.722,
          left: 1041.27,
          height: 2,
          width: 2,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 517.8,
          left: 1159.82,
          height: 1,
          width: 1,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 423.654,
          left: 1533.9,
          height: 3,
          width: 3,
          animationDuration: "6s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 725.451,
          left: 286.671,
          height: 2,
          width: 2,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 560.359,
          left: 478.679,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 778.03,
          left: 335.356,
          height: 4,
          width: 4,
          animationDuration: "0s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 213.531,
          left: 1451.45,
          height: 2,
          width: 2,
          animationDuration: "8s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 576.043,
          left: 753.684,
          height: 1,
          width: 1,
          animationDuration: "0s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 641.058,
          left: 165.484,
          height: 1,
          width: 1,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 677.709,
          left: 64.669,
          height: 2,
          width: 2,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 561.578,
          left: 586.172,
          height: 3,
          width: 3,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 206.136,
          left: 48.2257,
          height: 2,
          width: 2,
          animationDuration: "6s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 746.157,
          left: 1405.29,
          height: 0,
          width: 0,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 211.812,
          left: 1336.75,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 397.064,
          left: 1020.41,
          height: 1,
          width: 1,
          animationDuration: "3s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 830.072,
          left: 645.815,
          height: 1,
          width: 1,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 507.993,
          left: 1508.48,
          height: 2,
          width: 2,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 136.236,
          left: 541.927,
          height: 1,
          width: 1,
          animationDuration: "4s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 286.873,
          left: 1065.96,
          height: 4,
          width: 4,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 756.757,
          left: 424.169,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 631.866,
          left: 860.511,
          height: 2,
          width: 2,
          animationDuration: "2s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 159.045,
          left: 293.591,
          height: 3,
          width: 3,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 261.717,
          left: 1262.99,
          height: 1,
          width: 1,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 941.3,
          left: 1159.75,
          height: 3,
          width: 3,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 221.773,
          left: 400.69,
          height: 3,
          width: 3,
          animationDuration: "5s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 588.743,
          left: 795.836,
          height: 1,
          width: 1,
          animationDuration: "7s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 454.029,
          left: 1216,
          height: 3,
          width: 3,
          animationDuration: "8s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 884.329,
          left: 585.651,
          height: 2,
          width: 2,
          animationDuration: "1s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 345.695,
          left: 476.256,
          height: 1,
          width: 1,
          animationDuration: "9s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 722.457,
          left: 1345.21,
          height: 1,
          width: 1,
          animationDuration: "7s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 646.32,
          left: 4.63107,
          height: 4,
          width: 4,
          animationDuration: "8s",
        }}
      ></StyleStar>
      <StyleStar
        data-v-c479c21e=""
        style={{
          top: 699.272,
          left: 244.548,
          height: 3,
          width: 3,
          animationDuration: "6s",
        }}
      ></StyleStar>
    </StyleWrapper>
  );
};

export default Star;
