import React from "react";
import {
  StyleWrapper,
  Styletab,
  StyleActiveTab,
  StyleTabContent,
  StyleTabContentScreen,
  StyleBtn,
} from "./style";
import {
  StyleTitle,
  StyleDescript,
  StyleContents,
} from "assets/style/globalstyle";

const Learn = () => {
  return (
    <StyleWrapper>
      <StyleTitle style={{ marginBottom: "30px" }}>Chúng tôi là ai</StyleTitle>
      <StyleDescript style={{ marginBottom: "30px" }}>
        <b>Chăm sóc</b> sức khoẻ. <b>Đánh thức</b> sắc đẹp
      </StyleDescript>
      <StyleContents>
        "Spa Be Nguyen, spa tuyệt đẹp tại Hội An. Chúng tôi tận tâm đáp ứng mọi
        nhu cầu của khách hàng, từ liệu pháp mát-xa đến dịch vụ gội đầu và làm
        móng. Với chất lượng cao nhất và giá cả phải chăng, chúng tôi luôn đặt
        mục tiêu mang đến trải nghiệm thư giãn tuyệt vời và nâng cao sức khỏe và
        vẻ đẹp tự nhiên cho cộng đồng."
      </StyleContents>
    </StyleWrapper>
  );
};
export default Learn;
