import React from "react";
import { AiFillStar, AiFillGithub } from "react-icons/ai";
import {
  BannerContainer,
  BannerContent,
  BannerTitle,
  BannerDescription,
  FadeInText,
  fadeIn,
  StyleButton,
  StyleButtonStart,
} from "./style";
import Typist from "react-typist";
import { useNavigate } from "react-router-dom";
const Banner = () => {
  const navigate = useNavigate();
  const titleText = "Welcome to Home Spa";
  const titleWords = titleText.split(" ");
  return (
    <BannerContainer>
      <BannerContent>
        <BannerTitle>
          <span>Chào mừng đến với Be Nguyen Spa</span>
        </BannerTitle>
        <BannerDescription>
          {Array.from(
            "Thư giãn và làm mới giác quan của bạn với dịch vụ spa chúng tôi."
          ).map((char, index) => (
            <FadeInText key={index} delay={index / 10}>
              {char}
            </FadeInText>
          ))}
        </BannerDescription>
        {/* Add any additional content for the banner here */}
        <StyleButton>
          <div onClick={() => navigate("/contact")}>Liên hệ</div>
          <div
            onClick={() => navigate("/about")}
            style={{
              marginLeft: "20px",
              border: "1px solid #D4AA30",
              color: "#D4AA30",
            }}
            onMouseEnter={(e) => {
              e.target.style.backgroundColor = "#D4AA30";
              e.target.style.color = "#fff";
            }}
            onMouseLeave={(e) => {
              e.target.style.backgroundColor = null;
              e.target.style.color = "#D4AA30";
            }}
          >
            Chi tiết về chúng tôi
          </div>
        </StyleButton>
      </BannerContent>
    </BannerContainer>
  );
};

export default Banner;
