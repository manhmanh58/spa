import styled, { keyframes } from 'styled-components';
import { devices } from "assets/style/responsive";
import bannerImg from 'assets/images/5.jpg';
import bg from 'assets/images/bg.png';

const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const FadeInText = styled.span`
  opacity: 0;
  animation: ${fadeIn} 0.8s ease-in-out forwards;
  animation-delay: ${props => props.delay}s;
`;
const BannerContainer = styled.div`
  background-image: url(${bannerImg});
  background-size: cover;
  background-position: center;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
 
`;

const BannerContent = styled.div`
  text-align: center;
  color: white;
`;

const BannerTitle = styled.h1`
  font-size: 4rem;
  margin-bottom: 1rem;
  position: relative;
  animation: ${keyframes`
    0% {
      transform: translateY(-100%);
      opacity: 0;
    }
    100% {
      transform: translateY(0);
      opacity: 1;
    }
  `} 1s ease forwards;
  opacity: 0;

  > span {
    display: inline-block;
  }
`;
const BannerDescription = styled.p`
  font-size: 1.5rem;
  margin-bottom: 1rem;
  font-family: 'Roboto', sans-serif;
  white-space: pre-wrap;
`;
const StyleButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  @media ${devices.iphone} {
    flex-direction: column;
    a + button {
      margin-top: 20px;
    }
  }
  button {
    margin-left: 20px;
  }
  div {
    display: flex;
    align-items: center;
    cursor: pointer;
    padding: 11px;
    border-radius: 6px;
    border: 1px solid #fff;
    color: #fff;
    text-decoration: none;
    &:hover {
      background-color: #6F6F6F;
    }
    svg {
      height: 16px;
      margin-right: 10px;
    }
  }
`;
const StyleButtonStart = styled.button`
  background-color: #00dc82;
  border: none;
  border-radius: 5px;
  color: #000;
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  outline: none;
  padding: 10px 16px;
  &:hover {
    background-color: #4de7a8;
  }
`;
export { BannerContainer,BannerContent ,BannerTitle,BannerDescription,FadeInText,fadeIn,StyleButton,StyleButtonStart};
