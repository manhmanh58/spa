import styled from "styled-components";

const StyleWrapper = styled.div`
  position: absolute;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  top: 24px;
`;

const StyleStar = styled.div`
  border-radius: 50%;
  background-color: #fff;
  position: absolute;
`;

export { StyleWrapper, StyleStar };
