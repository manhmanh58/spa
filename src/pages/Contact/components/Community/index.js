import React from "react";
import {
  StyleWrapper,
  StyleContainer,
  StyleContent,
  StyleLink,
  StyleText,
  StyleName,
} from "./style";
import { AiOutlineRight } from "react-icons/ai";
import { COMUNITIES } from "constants/staticData";
import { StyleTitle, StyleDescript } from "assets/style/globalstyle";
const Community = () => {
  return (
    <StyleWrapper>
      <StyleTitle>Community</StyleTitle>
      <StyleDescript>
        Sharing is <b>Caring</b>
      </StyleDescript>
      <StyleContent>
        Discover articles from the framework team and community about Nuxt. Tips
        and tricks included!
      </StyleContent>
      <StyleContainer>
        {COMUNITIES.map((community, index) => (
          <StyleContent key={index}>
            <div style={{ marginBottom: "10px" }}>
              <img src={community.img} alt="" />
            </div>
            <StyleName>{community.name}</StyleName>
            <StyleText>{community.title}</StyleText>
            <p style={{ fontSize: "18px" }}>
              {community.des.slice(0, 74) + "..."}
            </p>
            <StyleLink>
              Get Infos <AiOutlineRight />
            </StyleLink>
          </StyleContent>
        ))}
      </StyleContainer>
    </StyleWrapper>
  );
};

export default Community;
