import React from "react";
import { AiFillStar, AiFillGithub } from "react-icons/ai";
import {
  StyleAnnoun,
  StyleWrapper,
  StyleButton,
  StyleButtonStart,
} from "./style";

const Banner = () => {
  return (
    <StyleWrapper>
      <StyleAnnoun>
        <AiFillStar /> <b>Announcement:</b> Nuxt 3 Release Candidate is out!
      </StyleAnnoun>
      <h3>The Intuitive Vue Framework</h3>
      <h2 style={{ margin: "0" }}>
        Build your next Vue.js application with confidence using Nuxt.{" "}
      </h2>
      <h2>
        An open source framework making web development simple and powerful.
      </h2>

      <StyleButton>
        <a href="/">
          <AiFillGithub />
          40K+ GitHub stars
        </a>
        <StyleButtonStart>Get started</StyleButtonStart>
      </StyleButton>
    </StyleWrapper>
  );
};

export default Banner;
