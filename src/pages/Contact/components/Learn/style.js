import styled from "styled-components";
import { devices } from "assets/style/responsive";
const StyleWrapper = styled.div`
  text-align: center;
  padding-top: 5rem;
  padding-bottom: 5rem;
`;

const Styletab = styled.div`
  margin-top: 50px;
  margin-bottom: 50px;
  width: 100%;
  text-align: left;
  > span {
    font-size: 18px;
    color: #fafafa;
    font-weight: 400;
    position: relative;
    cursor: pointer;
  }
  span + span {
    margin-left: 42px;
  }
`;
const StyleActiveTab = styled.div`
  position: absolute;
  height: 2px;
  width: 30px;
  left: 0;
  bottom: -8px;
  background-color: #00dc82;
`;
const StyleTabContent = styled.div`
  display: flex;
  width: 100%;
  @media ${devices.desktop} {
    flex-direction: column;
  }
`;

const StyleTabContentIde = styled.div`
  background-color: #001e26;
  border-radius: 6px;
  width: 50%;
  @media ${devices.desktop} {
    width: 100%;
  }
`;

const StyleTabContentHeader = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  padding: 10px 10px 0px;
  border-bottom: 1px solid #fff;
  > div {
    position: absolute;
    left: 10px;
  }
  span {
    background-color: rgba(0, 220, 130, 1);
    border-radius: 9999px;
    height: 10px;
    margin-right: 0.5rem;
    width: 10px;
    display: inline-block;
  }
  p {
    font-size: 16px;
  }
`;

const StyleTabContentMain = styled.div`
  display: flex;
`;

const StyleTabContentMainIndex = styled.ul`
  border-right: 1px solid #fff;
  margin-top: 10px;
  padding-right: 10px;
`;

const StyleTabContentMainCode = styled.ul`
  text-align: left;
  margin-top: 10px;
  margin-left: 20px;
  color: rgba(248, 113, 113, 1);
`;
const StyleTabContentScreen = styled.div`
  display: flex;
  align-items: start;
  flex-direction: column;
  justify-content: center;
  background-color: #fff;
  border-radius: 6px;
  margin-bottom: -20px;
  margin-top: -20px;
  padding: 20px;
  padding-left: 60px;
  padding-right: 20px;
  width: 50%;
  @media ${devices.desktop} {
    margin-top: -1px;
    margin-bottom: 20px;
    width: 100%;
  }
  @media ${devices.ipad} {
    margin-top: -1px;
    margin-bottom: 0;
    width: 100%;
  }
  @media ${devices.iphone} {
    margin-top: -1px;
    margin-bottom: 0;
    padding: 70px 60px;
    padding-bottom: 90px;
    width: 100%;
  }
  img {
    margin-top: 10px;
    display: block;
    &:first-of-type {
      width: 36px;
      height: 36px;
    }
    &:nth-of-type(2),
    &:nth-of-type(3) {
      max-width: 90%;
      width: auto;
    }
  }
`;
const StyleBtn = styled.div`
  text-align: left;
  margin-top: 20px;
  margin-bottom: 50px;
  button {
    padding: 10px 16px;
    font-size: 14px;
    color: #000;
    outline: none;
    border-radius: 5px;
    font-weight: 500;
    cursor: pointer;
    border: 1px solid rgba(0, 53, 67, 1);
    background-color: #00dc82;
    &:hover {
      background-color: #4de7a8;
    }
  }
`;

export {
  StyleWrapper,
  Styletab,
  StyleActiveTab,
  StyleTabContent,
  StyleTabContentIde,
  StyleTabContentHeader,
  StyleTabContentMain,
  StyleTabContentMainIndex,
  StyleTabContentMainCode,
  StyleTabContentScreen,
  StyleBtn,
};
