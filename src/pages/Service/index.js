import { StyleNuxtLink, StyleImage } from "./styles";
import Header from "components/Header";
import BlackBackGround from "components/BlackBackGround";
import LightBackground from "components/LightBackground";
import Nobackground from "components/NoBackground";
import Star from "pages/Home/components/Star";
import Planet from "pages/Home/components/Planet";
import Banner from "pages/Home/components/Banner";
import landscape from "assets/images/landscape.svg";
import Container from "components/Container";
import Learn from "pages/Home/components/Learn";
import landscape_white from "assets/images/landscape_white.svg";
import Features from "pages/Home/components/Features";
import SkyBackground from "components/SkyBackground";
import Partners from "pages/Home/components/Partners";
import Guide from "pages/Home/components/Guide";
import Explore from "pages/Home/components/Explore";
import Community from "pages/Home/components/Community";
import Testimonials from "pages/Home/components/Testimonials";
import Footer from "components/Footer";
import {
  FEATURES_IMAGE,
  NUXT_LINK,
  PARTNERS_IMAGE,
  GUIDE_IMAGE,
  EXPLORE_IMAGE,
  TESTIMONIALS_IMAGE,
} from "constants/home";

const Service = () => {
  return (
    <>
  
      <Header />
      <BlackBackGround mgTop="-72px">
        <Star />
        <Planet />
        <Container>
          <Banner />
          <StyleImage style={{ marginTop: "-13rem" }} src={landscape} alt="" />
        </Container>
      </BlackBackGround>

      <LightBackground>
        <Container>
          <Learn />
          <StyleImage src={landscape_white} alt="" />
        </Container>
      </LightBackground>
      <Nobackground>
        <Container>
          <Features />
          <StyleImage
            style={{
              transform: "translatey(15%)",
            }}
            src={FEATURES_IMAGE}
            alt=""
          />
        </Container>
      </Nobackground>
      <SkyBackground>
        <Container>
          <Partners />
          <StyleImage
            style={{
              transform: "translatey(15%)",
            }}
            src={PARTNERS_IMAGE}
            alt=""
          />
        </Container>
      </SkyBackground>
      <BlackBackGround style={{ marginTop: "200px" }}>
        <Container>
          <Guide />
          <StyleImage src={GUIDE_IMAGE} alt="" />
        </Container>
      </BlackBackGround>
      <Nobackground>
        <Container>
          <Explore />
          <StyleImage
            style={{
              transform: "translatey(3%)",
            }}
            src={EXPLORE_IMAGE}
            alt=""
          />
        </Container>
      </Nobackground>
      <LightBackground>
        <Container>
          <Community />
        </Container>
      </LightBackground>
      <Nobackground>
        <Container>
          <Testimonials />
          <StyleImage
            style={{
              transform: "translatey(0%)",
            }}
            src={TESTIMONIALS_IMAGE}
            alt=""
          />
        </Container>
      </Nobackground>
      <LightBackground>
        <Container>
          <Footer />
        </Container>
      </LightBackground>
    </>
  );
};

export default Service;
