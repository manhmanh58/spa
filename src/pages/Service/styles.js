import styled from "styled-components";
import { devices } from "assets/style/responsive";
const StyleNuxtLink = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgb(0, 220, 130);
  :hover {
    background-color: #4de7a8;
  }

  p {
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: #000;
    @media ${devices.lgPhone} {
      margin: 0;
      padding-left: 22px;
      text-align: start;
    }
  }
`;
const StyleImage = styled.img`
  width: 100%;
  position: absolute;
  object-fit: fill;
  display: block;
  left: 0;
  bottom: 0;
  transform: translatey(50%);
`;
export { StyleNuxtLink, StyleImage };
