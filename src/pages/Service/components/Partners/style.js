import styled from "styled-components";
import { devices } from "assets/style/responsive";

const StyleContainer = styled.div`
  grid-column-gap: 20px;
  column-gap: 20px;
  display: grid;
  grid-template-columns: auto auto;
  @media ${devices.lgPhone} {
    grid-template-columns: auto;
  }
`;
const StyleContent = styled.div`
  padding: 64px 64px 96px;
  @media ${devices.lgPhone} {
    &:first-of-type {
      margin-top: 50px;
    }
    padding: 32px 12px 48px;
    &:last-of-type {
      margin-bottom: 50px;
    }
  }
  img {
    margin-bottom: 20px;
  }
  p {
    margin-bottom: 20px;
  }
`;
const StyleBtn = styled.div`
  border: none;
  background-color: #00dc82;

  border-radius: 5px;
  color: #000;
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  outline: none;
  padding: 10px 16px;
  &:hover {
    background-color: #4de7a8;
  }
`;
const StyleText = styled.p`
  font-size: 1.25rem;
  line-height: 1.75rem;
  font-weight: 700;
`;
const StyleButton = styled.button`
  background-color: transparent;

  border: 1px solid #003543;
  border-radius: 5px;
  color: #000;
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  outline: none;
  padding: 10px 16px;
  &:hover {
    color: rgba(0, 0, 0, 0.705);
  }
`;

export { StyleContainer, StyleContent, StyleBtn, StyleButton, StyleText };
