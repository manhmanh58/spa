import React from "react";
import {
  StyleContainer,
  StyleContent,
  StyleBtn,
  StyleButton,
  StyleText,
} from "./style";
import {
  StyleTitle,
  StyleDescript,
  StyleContents,
  StyleWrapper,
} from "assets/style/globalstyle";
import { PARTNERS } from "constants/staticData";
const Partners = () => {
  return (
    <StyleWrapper>
      <StyleTitle>Partners</StyleTitle>
      <StyleDescript>
        Sustainable<b> Development</b>
      </StyleDescript>
      <StyleContents style={{ fontSize: "20px" }}>
        Nuxt development is carried out by passionate developers, but the amount
        of effort needed to maintain and develop new features is not sustainable
        without proper financial backing. We are thankful for our sponsors and
        partners, who help make Nuxt possible.
      </StyleContents>
      <StyleContainer>
        {PARTNERS.map((partner, index) => (
          <StyleContent key={index}>
            <img src={partner.img} alt="" />
            <StyleText>{partner.title}</StyleText>
            <p>{partner.des}</p>
            <StyleButton>{partner.to}</StyleButton>
          </StyleContent>
        ))}
      </StyleContainer>
      <StyleBtn>Become a partner</StyleBtn>
    </StyleWrapper>
  );
};

export default Partners;
