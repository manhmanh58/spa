import styled from "styled-components";
import { devices } from "assets/style/responsive";

const StyleAnnoun = styled.span`
  background-color: rgba(0, 220, 130, 0.2);
  padding: 12px 16px;
  font-size: 16px;
  border-radius: 3px;
  border: 1px solid rgb(0, 220, 130);
  margin-bottom: 50px;
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const StyleWrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-bottom: 14rem;
  padding-top: 12rem;
  position: relative;
  text-align: center;
  z-index: 10;
  @media ${devices.iphone} {
    padding-top: 14rem;
  }
  h3 {
    margin: 0;
    margin-bottom: 24px;
    font-weight: 700;
    font-size: 4.5rem;
    @media ${devices.ipad} {
      font-size: 3.75rem;
      line-height: 3.75rem;
    }
    @media ${devices.iphone} {
      font-size: 2.25rem;
      line-height: 2.5rem;
    }
  }
  h2 {
    margin: 0;
    font-size: 1.5rem;
    line-height: 2rem;
    color: #b2cccc;
    font-weight: 600;
    margin-bottom: 24px;
    @media ${devices.ipad} {
      font-size: 1.25rem;
      line-height: 1.75rem;
    }
    @media ${devices.iphone} {
      font-size: 1rem;
      line-height: 1.5rem;
      margin-bottom: 32px;
    }
  }
`;
const StyleFrame = styled.h3`
  margin-bottom: 24px;
`;

const StyleButton = styled.div`
  display: flex;
  align-items: center;
  @media ${devices.iphone} {
    flex-direction: column;
    a + button {
      margin-top: 20px;
    }
  }
  button {
    margin-left: 20px;
  }
  a {
    display: flex;
    align-items: center;
    cursor: pointer;
    padding: 11px;
    border-radius: 6px;
    color: #fff;
    text-decoration: none;
    &:hover {
      background-color: rgba(0, 53, 67, 1);
    }
    svg {
      height: 16px;
      margin-right: 10px;
    }
  }
`;
const StyleButtonStart = styled.button`
  background-color: #00dc82;
  border: none;
  border-radius: 5px;
  color: #000;
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  outline: none;
  padding: 10px 16px;
  &:hover {
    background-color: #4de7a8;
  }
`;
export { StyleAnnoun, StyleWrapper, StyleButton, StyleFrame, StyleButtonStart };
