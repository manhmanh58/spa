import React from "react";
import {
  StyleWrapper,
  Styletab,
  StyleActiveTab,
  StyleTabContent,
  StyleTabContentIde,
  StyleTabContentHeader,
  StyleTabContentMain,
  StyleTabContentMainCode,
  StyleTabContentMainIndex,
  StyleTabContentScreen,
  StyleBtn,
} from "./style";
import {
  StyleTitle,
  StyleDescript,
  StyleContents,
} from "assets/style/globalstyle";
import content_1 from "assets/images/content_1.svg";
import content_2 from "assets/images/content_2.svg";
import content_3 from "assets/images/content_3.svg";

const Learn = () => {
  let content = [];
  for (let i = 1; i < 10; i++) {
    content.push(<li key={i}>{i}</li>);
  }
  return (
    <StyleWrapper>
      <StyleTitle>Learn</StyleTitle>
      <StyleDescript>
        <b>Easy</b> to learn. <b>Easy</b> to master
      </StyleDescript>
      <StyleContents>
        Learn everything you need to know, from beginner to master.
      </StyleContents>
      <Styletab>
        <span>
          From CLI<StyleActiveTab></StyleActiveTab>
        </span>
        <span>From Scratch</span>
      </Styletab>
      <StyleTabContent>
        <StyleTabContentIde>
          <StyleTabContentHeader>
            <div>
              <span></span>
              <span></span>
              <span></span>
            </div>
            <p>index.vue</p>
          </StyleTabContentHeader>
          <StyleTabContentMain>
            <StyleTabContentMainIndex>{content}</StyleTabContentMainIndex>
            <StyleTabContentMainCode>
              <li>&lt;template&gt;</li>
              <li style={{ marginLeft: 5 }}>&lt;div&gt;</li>
              <li style={{ marginLeft: 10 }}>&lt;Logo/&gt;</li>
              <li style={{ marginLeft: 10 }}>&lt;NuxtCard/&gt;</li>
              <li style={{ marginLeft: 10 }}>&lt;NuxtCard/&gt;</li>
              <li style={{ marginLeft: 10 }}>&lt;AlertBanner/&gt;</li>
              <li style={{ marginLeft: 5 }}>&lt;div/&gt;</li>
              <li>&lt;template&gt;</li>
              <li></li>
            </StyleTabContentMainCode>
          </StyleTabContentMain>
        </StyleTabContentIde>
        <StyleTabContentScreen>
          <img src={content_1} alt="" />
          <img src={content_2} alt="" />
          <img src={content_3} alt="" />
        </StyleTabContentScreen>
      </StyleTabContent>
      <StyleBtn>
        <button>Start learning</button>
      </StyleBtn>
    </StyleWrapper>
  );
};
export default Learn;
