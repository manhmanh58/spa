import React from "react";
import { StyleContent, StyleContainer } from "./style";
import { EXPLORES } from "constants/staticData";
import {
  StyleTitle,
  StyleDescript,
  StyleContents,
  StyleWrapper,
} from "assets/style/globalstyle";

const Explore = () => {
  return (
    <StyleWrapper>
      <StyleTitle>Explore </StyleTitle>
      <StyleDescript>
        Moving forward? So much to <b> Explore </b>
      </StyleDescript>
      <StyleContents>
        Discover powerful modules, integrate with your favorite providers and
        start quickly with themes.
      </StyleContents>
      <StyleContainer>
        {EXPLORES.map((explore, index) => (
          <StyleContent key={index}>
            <img src={explore.img} alt="" />
            <p style={{ fontSize: "20px", fontWeight: "700" }}>
              {explore.title}
            </p>
            <p>{explore.des}</p>
          </StyleContent>
        ))}
      </StyleContainer>
    </StyleWrapper>
  );
};

export default Explore;
