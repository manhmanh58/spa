import styled from "styled-components";
import { devices } from "assets/style/responsive";

const StyleContent = styled.div`
  margin-top: 50px;
  display: grid;
  column-gap: 48px;
  row-gap: 48px;
  grid-template-columns: auto auto auto auto;
  margin-bottom: 100px;
  img {
    margin-bottom: 20px;
    @media ${devices.iphone} {
      margin-bottom: 0;
    }
  }
  p {
    margin-top: 10px;
    @media ${devices.iphone} {
      margin: 0;
    }
  }
  @media ${devices.desktop} {
    grid-template-columns: auto auto auto;
  }
  @media ${devices.ipad} {
    grid-template-columns: auto auto auto;
  }
  @media ${`(max-width: 767px)`} {
    grid-template-columns: auto auto;
    row-gap: 20px;
  }
  @media ${devices.iphone} {
    grid-template-columns: auto;
  }
`;
const StyleText = styled.p`
  font-size: 1.25rem;
  line-height: 1.75rem;
  font-weight: 700;
`;
export { StyleContent, StyleText };
