import React from "react";
import { StyleContent, StyleText } from "./style";
import { FEATURES } from "constants/staticData";
import {
  StyleTitle,
  StyleDescript,
  StyleContents,
  StyleWrapper,
} from "assets/style/globalstyle";

const Features = () => {
  return (
    <StyleWrapper>
      <StyleTitle>Features</StyleTitle>
      <StyleDescript>
        Intuitive <b>D</b>eveloper E<b>x</b>perience
      </StyleDescript>
      <StyleContents>
        Nuxt is shipped with plenty of features to boost developer productivity
        and the end user experience.
      </StyleContents>
      <StyleContent>
        {FEATURES.map((feat, index) => (
          <div key={index}>
            <img src={feat.img} alt="" />
            <StyleText>{feat.title}</StyleText>
            <p style={{ fontSize: "16px" }}>{feat.des}</p>
          </div>
        ))}
      </StyleContent>
    </StyleWrapper>
  );
};

export default Features;
